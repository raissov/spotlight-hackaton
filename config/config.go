package config

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

type Config struct {
	Port            string `yaml:"port"`
	ShutdownTimeout int    `yaml:"shutdown_timeout"`
	DBHost          string `yaml:"db_host"`
	DBPort          int    `yaml:"db_port"`
	DBUser          string `yaml:"db_user"`
	DBPassword      string `yaml:"db_password"`
	DBName          string `yaml:"db_name"`
	DBTimeout       int    `yaml:"db_timeout"`
}

func New() (*Config, error) {
	yamlBytes, err := os.ReadFile("./config.yml")
	if err != nil {
		log.Fatal(err)
	}
	// parse the YAML stored in the byte slice into the struct
	config := &Config{}
	err = yaml.Unmarshal(yamlBytes, config)
	if err != nil {
		log.Fatal(err)
	}
	return config, err
}
