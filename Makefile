# TODO: change project name
PROJECT = hackaton
MAIN = cmd/main.go
GIT= gitlab.com/raissov/spotlight-hackaton

# build - target for building go application
build: $(MAIN)
	go build -v -ldflags '-w -s' -o bin/$(PROJECT) $(MAIN)

# docker - target for building docker image with go application
# Need WT_TAG environment variable
docker:
ifndef WT_TAG
	$(error WT_TAG is undefined)
endif
	docker build -t $(REGISTRY)/$(PROJECT):$(WT_TAG) -f build/Dockerfile .

# clean - target for cleaning project
clean: clean-doc
	go clean
	rm -rf bin/$(PROJECT)

# doc - target for generating project documentation
doc:
	@for d in $(shell find internal -type d); do echo "Generate doc for $(PWD)/$${d}"; mkdir -p docs/$${d}; godoc -url "http://localhost:6060/pkg/$(GIT)/$(PROJECT)/$${d}/?m=all" > docs/$${d}/index.html; sed -r -i "s|\\\"(.*)\\/\\?m=all|\\\"$(PWD)\\/docs\\/$${d}\\/\\1\\/index.html\\/\\?m=all|g" docs/$${d}/index.html; sed -r -i "s|\\/lib\\/godoc|$(PWD)\\/docs\\/godoc|g" docs/$${d}/index.html;  done

# clean-doc - target for cleaning all automatic generated documentation
clean-doc:
	rm -rf docs/internal

lint:
	golangci-lint run -E revive -E gosec -E stylecheck -E bodyclose -E cyclop -E gofmt -D structcheck

test:
	go test -v ./...