package models

import (
	"context"
	"time"
)

type ChatMessage struct {
	ID        int       `json:"id"`
	ChatID    int       `json:"chat_id"`
	SenderID  int       `json:"sender_id"`
	Content   string    `json:"content"`
	IsRead    bool      `json:"is_read"`
	CreatedAt time.Time `json:"created_at"`
}
type Chat struct {
	ID           int `json:"id"`
	FirstUserID  int `json:"first_user_id"`
	SecondUserID int `json:"second_user_id"`
}

type ChatFilter struct {
	// Filtering fields.
	ID           *int
	FirstUserId  *int
	SecondUserId *int

	SpecifiedUser *int

	// Restrict to subset of results.
	Offset int `json:"offset"`
	Limit  int `json:"limit"`
}

type ChatMessageFilter struct {
	// Filtering fields.
	ID        *int       `json:"id"`
	ChatID    *int       `json:"chat_id"`
	SenderID  *int       `json:"sender_id"`
	Content   *string    `json:"content"`
	IsRead    *bool      `json:"is_read"`
	CreatedAt *time.Time `json:"created_at"`

	// Restrict to subset of results.
	Offset int `json:"offset"`
	Limit  int `json:"limit"`

	OrderBy *string `json:"order_by"`
	Order   *string `json:"order"`

	BeforeTime *time.Time `json:"before_time"`
}

type ChatMessageUpdate struct {
	Content *string `json:"content"`
	IsRead  *bool   `json:"is_read"`
}

type ChatMessageValidator struct {
	Content *string `json:"content"`
	IsRead  *bool   `json:"is_read"`
}

const (
	MaxUsernameLen    = 150
	MaxEmailLen       = 150
	MaxPhoneNumberLen = 128
)

// User represents a user in the system.
type User struct {
	ID    int    `json:"id"`
	Email string `json:"email"`
}

const (
	NumOfRecentMessages = 4
	ColumnCreatedAt     = "created_at"
	ASC                 = "ASC"
	DESC                = "DESC"
)

type contextKey int

const (
	// Stores the current logged in user in the context.
	userContextKey = contextKey(iota + 1)

	// Stores the "flash" in the context. This is a term used in web development
	// for a message that is passed from one request to the next for informational
	// purposes. This could be moved into the "http" package as it is only HTTP
	// related but both the "http" and "http/html" packages use it so it is
	// easier to move it to the root.
	flashContextKey

	// Stores the token that authenticates the operations with auth providers.
	tokenContextKey
)

func NewContextWithUser(ctx context.Context, user *User) context.Context {
	return context.WithValue(ctx, userContextKey, user)
}

func UserFromContext(ctx context.Context) *User {
	user, _ := ctx.Value(userContextKey).(*User)
	return user
}

const (
	MaxChatMessageLength = 250
	MaxLimitChatMessages = 15
)

const ChatsPegPage = 10

type ChatContainer struct {
	Chat        *Chat
	User        *User
	LastMessage *ChatMessage
	Unread      int
}
