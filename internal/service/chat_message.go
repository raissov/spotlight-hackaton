package service

import (
	"hackaton/internal/models"
	"hackaton/internal/repository"
)

type ChatMessageService struct {
	Chat repository.ChatMessage
}

func newChatMessageService(chat repository.ChatMessage) *ChatMessageService {
	return &ChatMessageService{
		Chat: chat,
	}
}

func (c *ChatMessageService) CreateMessage(chatMessage *models.ChatMessage) error {
	return c.CreateMessage(chatMessage)
}

func (c *ChatMessageService) ReadAllMessagesByChatID(chatId, senderID int) error {
	if err := c.Chat.ReadAllMessagesByChatID(chatId, senderID); err != nil {
		return err
	}
	return nil
}

func (c *ChatMessageService) UpdateChatMessageByID(id int, upd models.ChatMessageUpdate) (*models.ChatMessage, *models.ChatMessageValidator, error) {
	msg, ev, err := c.UpdateChatMessageByID(id, upd)
	if err != nil {
		return nil, nil, err
	}
	return msg, ev, err
}

func (c *ChatMessageService) FindRecentChatMessageByChatID(chatID int) (*models.ChatMessage, error) {
	chatMsg, err := c.findLastChatMessageByChatID(chatID)
	if err != nil {
		return nil, err
	}
	return chatMsg, nil
}

func (c *ChatMessageService) FindRecentChatMessages(userId int) ([]*models.ChatMessage, int, error) {
	messages, n, err := c.FindRecentChatMessages(userId)
	if err != nil {
		return nil, 0, err
	}
	return messages, n, nil

}

func (c *ChatMessageService) FindChatMessages(filter *models.ChatMessageFilter) ([]*models.ChatMessage, int, error) {
	return c.Chat.FindChatMessages(filter)
}

func (c *ChatMessageService) FindChatMessageByID(id int) (*models.ChatMessage, error) {
	chatMsg, err := c.findChatMessageByID(id)
	if err != nil {
		return nil, err
	}
	return chatMsg, nil
}

func (c *ChatMessageService) FindChatMessagesByChatID(chatID int) ([]*models.ChatMessage, error) {
	return c.Chat.FindChatMessagesByChatID(chatID)

}

func (c *ChatMessageService) findLastChatMessageByChatID(chatID int) (*models.ChatMessage, error) {
	column := models.ColumnCreatedAt
	order := models.DESC
	chatMessages, _, err := c.FindChatMessages(&models.ChatMessageFilter{ChatID: &chatID, OrderBy: &column, Order: &order, Limit: 1})
	if err != nil {
		return nil, err
	} else if len(chatMessages) == 0 {
		return nil, err
	}
	return chatMessages[0], nil
}

func (c *ChatMessageService) findChatMessagesByChatID(id int) ([]*models.ChatMessage, int, error) {
	return c.findChatMessagesByChatID(id)
}

func (c *ChatMessageService) findChatMessageByID(id int) (*models.ChatMessage, error) {
	chatMsg, err := c.Chat.FindChatMessageByID(id)
	if err != nil {
		return nil, err
	}
	return chatMsg, nil
}
