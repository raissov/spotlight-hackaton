package service

import (
	"hackaton/internal/models"
	"hackaton/internal/repository"
)

type UserService struct {
	Chat repository.User
}

func newUserService(chat repository.User) *UserService {
	return &UserService{
		Chat: chat,
	}
}

func (u *UserService) CreateUser(email string) (*models.User, error) {
	return u.CreateUser(email)
}

func (u *UserService) FindUserByEmail(email string) (*models.User, error) {
	return u.FindUserByEmail(email)
}

func (u *UserService) FindUserByID(id int) (*models.User, error) {
	return u.Chat.FindUserByID(id)
}
