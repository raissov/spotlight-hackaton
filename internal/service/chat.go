package service

import (
	"hackaton/internal/models"
	"hackaton/internal/repository"
)

type ChatService struct {
	Chat repository.Chat
}

func newChatService(chat repository.Chat) *ChatService {
	return &ChatService{
		Chat: chat,
	}
}

func (c *ChatService) FindChatByUserID(userID int) ([]*models.Chat, error) {
	chatMsg, err := c.Chat.FindChatByUserID(userID)
	if err != nil {
		return nil, err
	}
	return chatMsg, nil
}

func (c *ChatService) FindChatByID(id int) (*models.Chat, error) {
	chats, err := c.Chat.FindChatByID(id)
	if err != nil {
		return nil, err
	}
	return chats, nil
}

func (c *ChatService) FindChatByUserIDs(firstUserID, secondUserID int) (*models.Chat, error) {
	chat, err := c.Chat.FindChatByUserIDs(firstUserID, secondUserID)
	if err != nil {
		return nil, err
	}
	return chat, nil
}

func (c *ChatService) CreateChat(chat *models.Chat) error {
	if err := c.Chat.CreateChat(chat); err != nil {
		return err
	}
	return nil
}
