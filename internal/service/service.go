package service

import (
	"hackaton/internal/models"
	"hackaton/internal/repository"
)

type Service struct {
	Chat        Chat
	ChatMessage ChatMessage
	User        User
}

type Chat interface {
	FindChatByUserID(userID int) ([]*models.Chat, error)
	FindChatByID(id int) (*models.Chat, error)
	FindChatByUserIDs(firstUserID, secondUserID int) (*models.Chat, error)
	CreateChat(chat *models.Chat) error
}

type ChatMessage interface {
	ReadAllMessagesByChatID(chatId, senderID int) error
	UpdateChatMessageByID(id int, upd models.ChatMessageUpdate) (*models.ChatMessage, *models.ChatMessageValidator, error)
	FindRecentChatMessageByChatID(chatID int) (*models.ChatMessage, error)
	FindRecentChatMessages(userId int) ([]*models.ChatMessage, int, error)
	FindChatMessages(filter *models.ChatMessageFilter) ([]*models.ChatMessage, int, error)
	FindChatMessageByID(id int) (*models.ChatMessage, error)
	FindChatMessagesByChatID(chatID int) ([]*models.ChatMessage, error)
	CreateMessage(chatMessage *models.ChatMessage) error
	findLastChatMessageByChatID(chatID int) (*models.ChatMessage, error)
	findChatMessagesByChatID(id int) ([]*models.ChatMessage, int, error)
	findChatMessageByID(id int) (*models.ChatMessage, error)
}

type User interface {
	FindUserByEmail(email string) (*models.User, error)
	FindUserByID(id int) (*models.User, error)
	CreateUser(email string) (*models.User, error)
}

func New(repo *repository.Repositories) *Service {
	return &Service{
		Chat:        newChatService(repo.Chat),
		ChatMessage: newChatMessageService(repo.ChatMessage),
		User:        newUserService(repo.User),
	}
}
