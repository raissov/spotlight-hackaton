package repository

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
	"hackaton/internal/models"
)

type Repositories struct {
	User        User
	Chat        Chat
	ChatMessage ChatMessage
}

type User interface {
	CreateUser(email string) (*models.User, error)
	FindUserByEmail(email string) (*models.User, error)
	FindUserByID(id int) (*models.User, error)
}

type Chat interface {
	FindChatByUserID(userID int) ([]*models.Chat, error)
	FindChatByUserIDs(firstUserID, secondUserID int) (*models.Chat, error)
	CreateChat(chat *models.Chat) error
	FindRecentChat(filter *models.ChatFilter) ([]*models.Chat, int, error)
	FindChatByID(id int) (*models.Chat, error)
}

type ChatMessage interface {
	UpdateChatMessageByID(id int, upd *models.ChatMessageUpdate) (*models.ChatMessageUpdate, error)
	ReadAllMessagesByChatID(chatID, senderID int) error
	CreateMessage(chatMessage *models.ChatMessage) error
	FindChatMessages(filter *models.ChatMessageFilter) ([]*models.ChatMessage, int, error)
	FindRecentChatMessages(userID int) (_ []*models.ChatMessage, n int, err error)
	FindChatMessageByID(id int) (*models.ChatMessage, error)
	FindChatMessagesByChatID(chatID int) ([]*models.ChatMessage, error)
}

//type UserService interface {
//	FindUserByID(id int) (*User, error)
//	FindUserByEmail(email string) (*User, error)
//	FindUsers(filter UserFilter) ([]*User, int, error)
//	CreateUser(user *User) (*UserValidator, error)
//	UpdateUser(id int, upd UserUpdate) (*User, *UserValidator, error)
//	DeleteUser(id int) error
//}

func New(db *pgxpool.Pool, timeout int, logger *zap.Logger) *Repositories {
	return &Repositories{
		Chat:        newChatRepo(db, timeout, logger),
		ChatMessage: newChatMessageRepo(db, timeout, logger),
		User:        newUserRepo(db, timeout, logger),
	}
}
