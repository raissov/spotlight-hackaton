package repository

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
	"hackaton/internal/models"
	"time"
)

type UserRepository struct {
	db      *pgxpool.Pool
	timeout int
	logger  *zap.Logger
}

func newUserRepo(db *pgxpool.Pool, timeout int, logger *zap.Logger) *UserRepository {
	return &UserRepository{
		db:      db,
		timeout: timeout,
		logger:  logger,
	}
}

func (u *UserRepository) CreateUser(email string) (*models.User, error) {
	timeout := time.Duration(u.timeout)

	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()
	var ID int
	user := &models.User{}
	query := `INSERT INTO users (email) VALUES($1) RETURNING id`
	if err := u.db.QueryRow(ctx, query, email).Scan(&ID); err != nil {
		u.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	u.logger.Sugar().Infof("User Group successfully created with publicID: %d", ID)
	user.Email = email
	user.ID = ID
	return user, nil
}

func (u *UserRepository) FindUserByEmail(email string) (*models.User, error) {
	model := &models.User{}

	timeout := time.Duration(u.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT id, email FROM users WHERE id=$1`

	err := u.db.QueryRow(ctx, query, email).Scan(&model.ID, model.Email)
	if err != nil {
		u.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	return model, nil
}

func (u *UserRepository) FindUserByID(id int) (*models.User, error) {
	model := &models.User{}

	timeout := time.Duration(u.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT id, email FROM users WHERE id=$1`

	err := u.db.QueryRow(ctx, query, id).Scan(&model.ID, &model.Email)
	if err != nil {
		u.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	return model, nil
}
