package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"hackaton/config"
	"log"
	"time"
)

func NewPostgresPool(cfg *config.Config) (*pgxpool.Pool, error) {
	dbURI := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s", cfg.DBUser, cfg.DBPassword, cfg.DBHost, cfg.DBPort, cfg.DBName)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.DBTimeout)*time.Second)
	defer cancel()
	pool, err := pgxpool.Connect(ctx, dbURI)
	if err != nil {
		log.Printf("Error occured: %s", err)
		return nil, errors.New("database error")
	}
	log.Println("Connected to DB...")
	return pool, nil
}
