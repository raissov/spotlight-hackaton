package repository

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
	"hackaton/internal/models"
	"strconv"
	"strings"
	"time"
)

const (
	NumOfRecentMessages = 4
)

type ChatMessageRepository struct {
	db      *pgxpool.Pool
	timeout int
	logger  *zap.Logger
}

func newChatMessageRepo(db *pgxpool.Pool, timeout int, logger *zap.Logger) *ChatMessageRepository {
	return &ChatMessageRepository{
		db:      db,
		timeout: timeout,
		logger:  logger,
	}
}

func (c *ChatMessageRepository) UpdateChatMessageByID(id int, upd *models.ChatMessageUpdate) (*models.ChatMessageUpdate, error) {
	timeout := time.Duration(c.timeout)

	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		UPDATE chat_messages
		SET content = $1,
		    is_read = $2
		WHERE id = $3
	`
	res, err := c.db.Exec(ctx, query, upd.Content, upd.IsRead, id)
	if err != nil {
		c.logger.Sugar().Errorf("Error occured while processing to DB: %s", err.Error())
		return nil, err
	}
	count := res.RowsAffected()
	if count > 0 {
		c.logger.Sugar().Infof("Number of rows affected: %d", count)
		return nil, err
	}
	return upd, nil
}

func (c *ChatMessageRepository) ReadAllMessagesByChatID(chatID, senderID int) error {
	timeout := time.Duration(c.timeout)

	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		UPDATE chat_messages
		SET is_read = true
		WHERE chat_id = $1 AND sender_id = $2
	`
	res, err := c.db.Exec(ctx, query, chatID, senderID)
	if err != nil {
		c.logger.Sugar().Errorf("Error occured while processing to DB: %s", err.Error())
		return err
	}
	count := res.RowsAffected()
	if count > 0 {
		c.logger.Sugar().Infof("Number of rows affected: %d", count)
		return nil
	}
	return nil
}

func (c *ChatMessageRepository) CreateMessage(chatMessage *models.ChatMessage) error {
	var ID int64
	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		INSERT INTO chat_messages (
			chat_id,
			sender_id,
			content,
			is_read
		)
		VALUES ($1, $2, $3, false)
		RETURNING id
	`
	if err := c.db.QueryRow(ctx, query, chatMessage.ID, chatMessage.SenderID, chatMessage.Content).Scan(&ID); err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return err
	}
	c.logger.Sugar().Infof("Chat successfully created with publicID: %d", ID)

	return nil
}

func (c *ChatMessageRepository) FindChatMessages(filter *models.ChatMessageFilter) ([]*models.ChatMessage, int, error) {
	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	n := 0

	defer cancel()

	var args []interface{}
	where := []string{"1 = 1"}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v := filter.ChatID; v != nil {
		where, args = append(where, "chat_id = ?"), append(args, *v)
	}
	if v := filter.SenderID; v != nil {
		where, args = append(where, "sender_id = ?"), append(args, *v)
	}
	if v := filter.Content; v != nil {
		where, args = append(where, "content = ?"), append(args, *v)
	}
	if v := filter.IsRead; v != nil {
		where, args = append(where, "is_read = ?"), append(args, *v)
	}
	if v := filter.BeforeTime; v != nil {
		where, args = append(where, "created_at < ?"), append(args, *v)
	} else {
		where = append(where, "created_at < NOW()")
	}
	orderBy := "id"
	if v := filter.OrderBy; v != nil {
		orderBy = *v
	}
	order := "ASC"
	if v := filter.Order; v != nil {
		order = *v
	}
	query := `
		SELECT 
		    id,
		    chat_id,
		    sender_id,
			content,
			is_read,
			created_at,
		    COUNT(*) OVER()
		FROM chat_messages
		WHERE ` + strings.Join(where, " AND ") + `
		ORDER BY ` + orderBy + ` ` + order + `  
		` + FormatLimitOffset(filter.Limit, filter.Offset)

	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, n, err
	}
	defer rows.Close()
	var responses []*models.ChatMessage
	for rows.Next() {
		response := &models.ChatMessage{}
		if err = rows.Scan(&response.ID, &response.ChatID, &response.SenderID, &response.Content, &response.IsRead, &response.CreatedAt, &n); err != nil {
			c.logger.Sugar().Errorf("Error occurred while parsing DB data to models: %s", err.Error())
			return nil, n, err
		}
		responses = append(responses, response)
	}
	if err = rows.Err(); err != nil {
		c.logger.Sugar().Errorf("Database error: %s", err.Error())
		return nil, n, err
	}
	c.logger.Info("Success!")
	return responses, n, nil
}

func (c *ChatMessageRepository) FindChatMessagesByChatID(chatID int) ([]*models.ChatMessage, error) {
	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		SELECT 
		    id,
		    chat_id,
		    sender_id,
			content,
			is_read,
			created_at
		FROM chat_messages
		WHERE chat_id=$1`

	rows, err := c.db.Query(ctx, query, chatID)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	defer rows.Close()
	var responses []*models.ChatMessage
	for rows.Next() {
		response := &models.ChatMessage{}
		if err = rows.Scan(&response.ID, &response.ChatID, &response.SenderID, &response.Content, &response.IsRead, &response.CreatedAt); err != nil {
			c.logger.Sugar().Errorf("Error occurred while parsing DB data to models: %s", err.Error())
			return nil, err
		}
		responses = append(responses, response)
	}
	if err = rows.Err(); err != nil {
		c.logger.Sugar().Errorf("Database error: %s", err.Error())
		return nil, err
	}
	c.logger.Info("Success!")
	return responses, nil
}
func (c *ChatMessageRepository) FindChatMessageByID(id int) (*models.ChatMessage, error) {
	model := &models.ChatMessage{}

	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		SELECT 
		    id,
		    chat_id,
		    sender_id,
			content,
			is_read,
			created_at
		FROM chat_messages
		WHERE id=$1`

	err := c.db.QueryRow(ctx, query, id).Scan(&model.ID, &model.ChatID, &model.SenderID, &model.Content, &model.IsRead, &model.CreatedAt)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	return model, nil
}
func (c *ChatMessageRepository) FindRecentChatMessages(userID int) (_ []*models.ChatMessage, n int, err error) {
	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `
		SELECT m.id,
		   m.chat_id,	
		   m.sender_id,
		   m.content,
		   m.is_read,
		   m.created_at,
		   COUNT(*) OVER()
	FROM chat_messages m
	JOIN chats c on m.chat_id = c.id
	WHERE m.is_read = false AND m.sender_id <> ` + strconv.Itoa(userID) + ` AND (c.second_user_id = ` + strconv.Itoa(userID) + ` OR c.first_user_id = ` + strconv.Itoa(userID) + `)
	ORDER BY m.created_at DESC
	LIMIT ` + strconv.Itoa(NumOfRecentMessages) + `
;
`
	rows, err := c.db.Query(ctx, query)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, n, err
	}
	defer rows.Close()
	var responses []*models.ChatMessage
	for rows.Next() {
		var response *models.ChatMessage
		if err := rows.Scan(&response.ID, &response.ChatID, &response.SenderID, &response.Content, &response.IsRead, &response.CreatedAt, &n); err != nil {
			return nil, 0, err
		}
		responses = append(responses, response)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}
	return responses, n, nil

}
