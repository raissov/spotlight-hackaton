package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
	"hackaton/internal/models"
	"strconv"
	"time"
)

type ChatRepository struct {
	db      *pgxpool.Pool
	timeout int
	logger  *zap.Logger
}

func newChatRepo(db *pgxpool.Pool, timeout int, logger *zap.Logger) *ChatRepository {
	return &ChatRepository{
		db:      db,
		timeout: timeout,
		logger:  logger,
	}
}

func (c *ChatRepository) FindChatByID(id int) (*models.Chat, error) {
	model := &models.Chat{}

	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT id, first_user_id, second_user_id FROM chats WHERE id=$1`

	err := c.db.QueryRow(ctx, query, id).Scan(&model.ID, &model.FirstUserID, &model.SecondUserID)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	return model, nil
}

func (c *ChatRepository) FindChatByUserIDs(firstUserID, secondUserID int) (*models.Chat, error) {
	model := &models.Chat{}

	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT id, first_user_id, second_user_id FROM chats WHERE first_user_id=$1 AND second_user_id=$2`

	err := c.db.QueryRow(ctx, query, firstUserID, secondUserID).Scan(&model.ID, &model.FirstUserID, &model.SecondUserID)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	return model, nil
}

func (c *ChatRepository) FindChatByUserID(userID int) ([]*models.Chat, error) {

	var responses []*models.Chat
	timeout := time.Duration(c.timeout)
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT id, first_user_id, second_user_id FROM chats WHERE first_user_id = $1`

	rows, err := c.db.Query(ctx, query, userID)
	if err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		response := &models.Chat{}
		if err = rows.Scan(&response.ID, &response.FirstUserID, &response.SecondUserID); err != nil {
			c.logger.Sugar().Errorf("Error occurred while parsing DB data to models: %s", err.Error())
			return nil, err
		}
		responses = append(responses, response)
	}
	if err = rows.Err(); err != nil {
		c.logger.Sugar().Errorf("Database error: %s", err.Error())
		return nil, err
	}
	c.logger.Info("Success!")
	return responses, nil
}

func (c *ChatRepository) CreateChat(chat *models.Chat) error {
	if chat.FirstUserID == chat.SecondUserID {
		return errors.New("same ids for users")
	}
	var ID int
	timeout := time.Duration(c.timeout)

	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `INSERT INTO chats (first_user_id, second_user_id) VALUES($1, $2) RETURNING id`
	if err := c.db.QueryRow(ctx, query, chat.FirstUserID, chat.SecondUserID).Scan(&ID); err != nil {
		c.logger.Sugar().Errorf("Error occurred while querying to DB: %s", err.Error())
		return err
	}
	c.logger.Sugar().Infof("Chat successfully created with publicID: %d", ID)

	return nil
}

func (c *ChatRepository) FindRecentChat(filter *models.ChatFilter) (responses []*models.Chat, n int, err error) {
	timeout := time.Duration(c.timeout)

	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()

	query := `SELECT DISTINCT
		c.id,
		c.first_user_id,
		c.second_user_id,
		MAX(cm.created_at)
	FROM chats c
	JOIN chat_messages cm on c.id = cm.chat_id
	WHERE c.first_user_id = ` + strconv.Itoa(*filter.SpecifiedUser) + ` OR c.second_user_id = ` + strconv.Itoa(*filter.SpecifiedUser) + `
	GROUP BY c.id
	ORDER BY 4 DESC
	` + FormatLimitOffset(filter.Limit, filter.Offset)
	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return nil, n, err
	}
	defer rows.Close()

	for rows.Next() {
		response := &models.Chat{}
		var dummy time.Time
		if err := rows.Scan(
			&response.ID,
			&response.FirstUserID,
			&response.SecondUserID,
			&dummy,
		); err != nil {
			return nil, 0, err
		}
		responses = append(responses, response)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}
	query = `SELECT DISTINCT
	COUNT(DISTINCT c.id)
	FROM chats c
	JOIN chat_messages cm on c.id = cm.chat_id
	WHERE c.first_user_id = ` + strconv.Itoa(*filter.SpecifiedUser) + ` OR c.second_user_id = ` + strconv.Itoa(*filter.SpecifiedUser) + `
	ORDER BY cm.created_at DESC
	` + FormatLimitOffset(filter.Limit, filter.Offset)
	row := c.db.QueryRow(ctx, query)
	if row == nil {
		return nil, n, err
	}
	if err := row.Scan(
		&n,
	); err != nil {
		return nil, 0, err
	}
	return responses, n, nil

}

func FormatLimitOffset(limit, offset int) string {
	return "LIMIT" + string(rune(limit)) + "OFFSET" + string(rune(offset)) + ";"
}
