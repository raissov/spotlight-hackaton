package delivery

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"hackaton/internal/delivery/chat"
	"hackaton/internal/models"
	"hackaton/internal/service"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Handler struct {
	service *service.Service
	logger  *zap.Logger
}

func NewHandler(services *service.Service, logger *zap.Logger) *Handler {
	return &Handler{
		service: services,
		logger:  logger,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.Default()
	router.Use(GinMiddleware("http://localhost:8080"))
	router.GET("/chatws/:chat_id", h.handleChatWebSocket)
	router.GET("/chat", h.handleChats)
	router.GET("/chat/:chat_id", h.HandleChat)
	router.GET("/chat/reply/:user_id", h.HandleReply)
	router.GET("/chat/history/:chat_id", h.handleChatHistory)

	return router
}

func (h *Handler) handleChatHistory(c *gin.Context) {
	chatID, _ := strconv.Atoi(c.Param("chat_id"))
	if chatID < 0 {
		h.logger.Error("WTF")
		return
	}

	var filter *models.ChatMessageFilter
	beforeTime := c.Query("before_time")
	if beforeTime != "" {
		parsedTime, err := time.Parse("2006-01-02T15:04:05Z", beforeTime)
		if err != nil {
			h.logger.Error(err.Error())
			c.JSON(200, "Error while parsing time")
			return
		}
		filter.BeforeTime = &parsedTime
	}
	chats, err := h.service.ChatMessage.FindChatMessagesByChatID(chatID)
	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while parsing data")
		return
	}
	c.JSON(200, chats)
	h.logger.Info("Chat list returned!")
}

func (h *Handler) handleChatWebSocket(c *gin.Context) {
	// Parse chat ID from the path.
	chatId := c.Param("chat_id")
	h.logger.Info("handleChatWebSocket")
	userID, err := strconv.Atoi(c.Request.Header.Get("user_id"))
	if err != nil {
		log.Println("lol")
		return
	}
	user, err := h.service.User.FindUserByID(userID)
	chat.ServeWs(c.Writer, c.Request, chatId, user)
}

func (h *Handler) handleChats(c *gin.Context) {
	userID, _ := strconv.Atoi(c.Request.Header.Get("user_id"))
	chats, err := h.service.Chat.FindChatByUserID(userID)
	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while getting recent chats by filter")
		return
	}
	chatViews := make([]*models.ChatContainer, 0)

	if len(chats) != 0 {
		for _, v := range chats {
			senderID := v.FirstUserID
			if v.SecondUserID != userID {
				senderID = v.SecondUserID
			}
			var container models.ChatContainer
			container.Chat = v
			msg, err := h.service.ChatMessage.FindChatMessageByID(v.ID)
			if err != nil {
				h.logger.Error(err.Error())
				c.JSON(200, "Error occurred while getting recent chat message by filter")
				return
			} else {
				container.LastMessage = msg
			}
			user, err := h.service.User.FindUserByID(senderID)
			if err != nil {
				h.logger.Error(err.Error())
				c.JSON(200, "Error occurred while getting user")
				return
			}
			container.User = user
			chatViews = append(chatViews, &container)
		}
	}
	c.JSON(200, gin.H{
		"chat_container": chatViews,
	})
	h.logger.Info("Success!")
}

func (h *Handler) HandleChat(c *gin.Context) {
	chatID := c.Param("chat_id")
	numChatId, _ := strconv.Atoi(chatID)
	chatStruct, err := h.service.Chat.FindChatByID(numChatId)
	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while getting chat by ID: "+chatID)
		return
	}

	userID, _ := strconv.Atoi(c.Request.Header.Get("user_id"))

	recipientID := chatStruct.FirstUserID

	if userID == chatStruct.FirstUserID {
		recipientID = chatStruct.SecondUserID
	}
	recipient, err := h.service.User.FindUserByID(recipientID)

	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while getting user")
		return
	}
	currentUser, err := h.service.User.FindUserByID(userID)
	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while getting user")
		return
	}

	if err := h.service.ChatMessage.ReadAllMessagesByChatID(chatStruct.ID, userID); err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Error occurred while reading the chat"+chatID)
		return
	}
	c.JSON(200, gin.H{
		"recipient":       recipient,
		"chat":            chatStruct,
		"current_user":    currentUser,
		"connection_type": "ws",
	})
	h.logger.Info("Success!")
}

func (h *Handler) HandleReply(c *gin.Context) {
	userID := c.Param("user_id")
	numUserID, _ := strconv.Atoi(userID)
	replyID, _ := strconv.Atoi(c.Request.Header.Get("reply_id"))

	chatStruct, err := h.service.Chat.FindChatByUserIDs(numUserID, replyID)
	if err != nil {
		h.logger.Error(err.Error())
		c.JSON(200, "Could not find chat")
		return
	}
	if chatStruct == nil {
		chatStruct = &models.Chat{
			FirstUserID:  numUserID,
			SecondUserID: replyID,
		}
		err = h.service.Chat.CreateChat(chatStruct)
		if err != nil {
			h.logger.Error(err.Error())
			c.JSON(200, "Can't create chat :(")
			return
		}
	}
	log.Println(chatStruct)
	h.logger.Info(string(rune(chatStruct.FirstUserID)))
	c.JSON(200, gin.H{
		"redirectUrl": "/chat/" + strconv.Itoa(chatStruct.ID),
	})
	http.Redirect(c.Writer, c.Request, "/chat/"+strconv.Itoa(chatStruct.ID), 200)
}
func GinMiddleware(allowOrigin string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", allowOrigin)
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type, Content-Length, X-CSRF-Token, Token, session, Origin, Host, Connection, Accept-Encoding, Accept-Language, X-Requested-With")

		if c.Request.Method == http.MethodOptions {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}

		c.Request.Header.Del("Origin")

		c.Next()
	}
}
