package chat

import (
	"encoding/json"
	"fmt"
	"hackaton/internal/models"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// connection is an middleman between the websocket connection and the Hub.
type connection struct {
	// The websocket connection.
	ws *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *models.ChatMessage
}

// readPump pumps messages from the websocket connection to the Hub.
func (s subscription) readPump() {
	log.Println("cool")
	c := s.conn
	defer func() {
		HubVar.unregister <- s
		c.ws.Close()
	}()
	c.ws.SetReadLimit(maxMessageSize)
	c.ws.SetReadDeadline(time.Now().Add(pongWait))
	c.ws.SetPongHandler(func(string) error { c.ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, msg, err := c.ws.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
			}
			break
		}
		var chatMsg models.ChatMessage
		if err := json.Unmarshal(msg, &chatMsg); err != nil {
			return
		}
		HubVar.ChatMessageService.CreateMessage(&chatMsg)
		m := message{&chatMsg, s.room}
		HubVar.broadcast <- m
	}
}

// write writes a message with the given message type and payload.
func (c *connection) write(mt int, payload []byte) error {
	log.Println("cool")
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}

// writePump pumps messages from the Hub to the websocket connection.
func (s *subscription) writePump() {
	c := s.conn
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.ws.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if message.SenderID != s.user.ID {
				//isRead := true
				readErr := HubVar.ChatMessageService.ReadAllMessagesByChatID(message.ChatID, message.SenderID)
				if readErr != nil {
					return
				}
				//message = msg
			}
			var payload []byte
			var err error
			if payload, err = json.Marshal(message); err != nil {
				return
			}
			if err := c.write(websocket.TextMessage, payload); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func ServeWs(w http.ResponseWriter, r *http.Request, chatId string, user *models.User) {
	fmt.Print(chatId)
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}
	c := &connection{send: make(chan *models.ChatMessage, 256), ws: ws}
	s := subscription{c, chatId, user}
	HubVar.register <- s
	log.Println("wtf")
	go s.writePump()
	go s.readPump()
}
