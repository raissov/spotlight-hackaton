package chat

import (
	"hackaton/internal/models"
	"hackaton/internal/service"
	"log"
)

type message struct {
	chatMessage *models.ChatMessage
	room        string
}

type subscription struct {
	conn *connection
	room string
	user *models.User
}

// Hub maintains the set of active connections and broadcasts messages to the
// connections.
type Hub struct {
	// Registered connections.
	rooms map[string]map[*connection]bool

	// Inbound messages from the connections.
	broadcast chan message

	// Register requests from the connections.
	register chan subscription

	// Unregister requests from connections.
	unregister chan subscription

	ChatService        service.ChatService
	ChatMessageService service.ChatMessageService
}

var HubVar = Hub{
	broadcast:  make(chan message),
	register:   make(chan subscription),
	unregister: make(chan subscription),
	rooms:      make(map[string]map[*connection]bool),
}

func (h *Hub) Run() {
	for {
		select {
		case s := <-h.register:
			log.Println("lol")
			connections := h.rooms[s.room]
			if connections == nil {
				connections = make(map[*connection]bool)
				h.rooms[s.room] = connections
			}
			h.rooms[s.room][s.conn] = true
		case s := <-h.unregister:
			log.Println("lol")
			connections := h.rooms[s.room]
			if connections != nil {
				if _, ok := connections[s.conn]; ok {
					delete(connections, s.conn)
					close(s.conn.send)
					if len(connections) == 0 {
						delete(h.rooms, s.room)
					}
				}
			}
		case m := <-h.broadcast:
			log.Println("lol")
			connections := h.rooms[m.room]
			for c := range connections {
				select {
				case c.send <- m.chatMessage:
				default:
					close(c.send)
					delete(connections, c)
					if len(connections) == 0 {
						delete(h.rooms, m.room)
					}
				}
			}
		}
	}
}
