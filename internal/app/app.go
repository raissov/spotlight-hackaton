package app

import (
	"context"
	"go.uber.org/zap"
	"hackaton/config"
	delivery "hackaton/internal/delivery/http/v1"
	"hackaton/internal/repository"
	"hackaton/internal/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func Run() {
	configs, err := config.New()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("WTF", configs)
	postgresPool, err := repository.NewPostgresPool(configs)
	if err != nil {
		log.Println(err)
		return
	}
	defer postgresPool.Close()
	var logger *zap.Logger

	logger, err = zap.NewDevelopment()
	if err != nil {
		log.Println(err)
	}

	repo := repository.New(postgresPool, configs.DBTimeout, logger)
	service := service.New(repo)
	handlers := delivery.NewHandler(service, logger)
	srv := http.Server{
		Addr:    ":" + configs.Port,
		Handler: handlers.InitRoutes(),
	}

	go func() {
		logger.Sugar().Infof("Starting server on port: %s\n", configs.Port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Println(err)
			return
		}
	}()

	// Graceful Shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit

	logger.Debug("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(configs.ShutdownTimeout))
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Sugar().Debugf("Server forced to shutdown: %s", err)
	}
}
