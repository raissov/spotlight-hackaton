CREATE TABLE IF NOT EXISTS chat_messages
(
    id                      serial,
    chat_id                 int NOT NULL,
    sender_id               int NOT NULL,
    content                 text NOT NULL,
    is_read                 BOOLEAN      NOT NULL DEFAULT FALSE,
    created_at              TIMESTAMP    NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (id),
    FOREIGN KEY (sender_id) REFERENCES users(id),
    FOREIGN KEY (chat_id) REFERENCES chats(id)
);

CREATE TABLE IF NOT EXISTS chats
(
    id              serial,
    first_user_id   int NOT NULL,
    second_user_id  int NOT NULL,
    PRIMARY KEY (id),
    UNIQUE  KEY (first_user_id, second_user_id),
    FOREIGN KEY (first_user_id) REFERENCES users(id),
    FOREIGN KEY (second_user_id) REFERENCES users(id)
    );

CREATE TABLE IF NOT EXISTS users (
    id serial,
    email text UNIQUE NOT NULL ,
    PRIMARY KEY(id)
);
